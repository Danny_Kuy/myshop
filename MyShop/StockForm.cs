﻿using MyShopData;
using MyShopData.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyShop
{
    public partial class StockForm : Form, StockHelper.OnStockCallback
    {
        private StockHelper stockHelper;
        private int productId;
        private List<Category> categories = MyShopDataHelper.GetActiveCategories();
        public StockForm()
        {
            InitializeComponent();
            stockHelper = new StockHelper(this);
            CenterToScreen();
            foreach (MyShopData.Models.Category cat in categories)
            {
                comboBoxCategory.Items.Add(cat.CategoryName);
            }
            LoadData();
        }

        private void LoadData()
        {
            List<Product> products = MyShopDataHelper.GetProducts();
            dataGridView1.Rows.Clear();
            foreach (Product pro in products)
            {
                dataGridView1.Rows.Add(pro.ProductID, pro.ProductName, pro.ProductQty, pro.ProductPrice, pro.ProductCategoryId, pro.Currency, pro.CreateDate, pro.UpdateDate);
            }
            productId = 0;
            labelId.Text = "Code :";
            buttonAddProduct.Text = "Add Product";
            textBoxName.Text = "";
            textBoxQty.Text = "";
            textBoxPrice.Text = "";
            comboBoxCategory.SelectedIndex = -1;
            comboBoxCurrency.SelectedIndex = 0;
        }
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count != 0)
            {
                buttonAddProduct.Text = "Update";
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                productId = Convert.ToInt32(row.Cells[0].Value);
                textBoxName.Text = Convert.ToString(row.Cells[1].Value);
                textBoxQty.Text = Convert.ToString(row.Cells[2].Value);
                textBoxPrice.Text = Convert.ToString(row.Cells[3].Value);
                for(int i = 0;i <categories.Count;i++)
                {
                    if (categories[i].CategoryID == MyShopDataHelper.GetCategoryById(Convert.ToInt32(row.Cells[4].Value)).CategoryID)
                        comboBoxCategory.SelectedIndex = i;
                }
                comboBoxCurrency.Text = Convert.ToString(row.Cells[5].Value);
                labelId.Text = "Code : " + productId;
            }
        }
        private void buttonAddProduct_Click(object sender, EventArgs e)
        {
            stockHelper.AddProduct(productId, textBoxQty.Text, textBoxName.Text, comboBoxCategory, textBoxPrice.Text, categories, comboBoxCurrency.Text);
        }

        public void onAddProductCallback(bool isSuccess)
        {
            if (isSuccess)
            {
                LoadData();
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (productId > 0)
            {
                MyShopDataHelper.DeleteProduct(productId);
                LoadData();
            }
        }
    }
}
