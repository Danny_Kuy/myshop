﻿using MyShopData;
using MyShopData.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyShop
{
    public partial class CategoryForm : Form
    {
        private string categoryId;
        private string name;

        public CategoryForm()
        {
            InitializeComponent();
            CenterToParent();
            LoadData();
        }

        private void LoadData()
        {
            List<Category> categories = MyShopDataHelper.GetCategories();
            dataGridView1.Rows.Clear();
            foreach (Category cat in categories)
            {
                dataGridView1.Rows.Add(cat.CategoryID, cat.CategoryName, cat.Status);
            }
            labelId.Text = "Code :";
            textBoxName.Text = "";
            categoryId = "";
            comboBox1.SelectedIndex = 0;
            buttonAdd.Text = "Add";
        }


        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (IsValidate())
            {
                MyShopDataHelper.AddCategory(String.IsNullOrEmpty(categoryId) ? 0 : Int32.Parse(categoryId), name, comboBox1.Text);
                LoadData();
            }
        }

        private bool IsValidate()
        {
            name = textBoxName.Text;
            if (String.IsNullOrEmpty(name))
            {
                MessageBox.Show("Missing Category Name.");
                return false;
            }
            return true;
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(categoryId))
            {
                MyShopDataHelper.DeleteCategory(Int32.Parse(categoryId));
                LoadData();
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count != 0)
            {
                buttonAdd.Text = "Update";
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                textBoxName.Text = Convert.ToString(row.Cells[1].Value);
                comboBox1.Text = Convert.ToString(row.Cells[2].Value);
                categoryId = Convert.ToString(row.Cells[0].Value);
                labelId.Text = "Code : " + categoryId;
            }
        }
    }
}
