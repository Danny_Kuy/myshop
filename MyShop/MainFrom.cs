﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyShop
{
    public partial class MainFrom : Form
    {
        public MainFrom()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            
        }

        private void buttonStock_Click(object sender, EventArgs e)
        {
            new StockForm().Show();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            Application.Exit();
            
        }

        private void buttonCategory_Click(object sender, EventArgs e)
        {
            new CategoryForm().Show();
        }
    }
}
