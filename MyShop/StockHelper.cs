﻿using MyShopData;
using MyShopData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyShop
{
    public class StockHelper
    {
        private OnStockCallback onStockCallback;
    
        public StockHelper(OnStockCallback onStockCallback)
        {
            this.onStockCallback = onStockCallback;
        }
        public void AddProduct(int productId, String qty, String name, ComboBox category, String price, List<Category> categories, String currency)
        {
            if (IsValidate(qty, name, category.Text, price))
            {
                MyShopDataHelper.AddProduct(productId, name, Int32.Parse(qty), "", Double.Parse(price), categories[category.SelectedIndex].CategoryID, currency);
                onStockCallback.onAddProductCallback(true);
            }
            else
            {
                onStockCallback.onAddProductCallback(false);
            }
        }
        private bool IsValidate(String qty, String name, String category, String price)
        {
            if (String.IsNullOrEmpty(name))
            {
                MessageBox.Show("Missing Product Name.");
                return false;
            }
            if (String.IsNullOrEmpty(qty))
            {
                MessageBox.Show("Missing Product Qty.");
                return false;
            }
            if (String.IsNullOrEmpty(price))
            {
                MessageBox.Show("Missing Product Price.");
                return false;
            }
            if (String.IsNullOrEmpty(category))
            {
                MessageBox.Show("Missing Product Category.");
                return false;
            }
            return true;
        }

        public interface OnStockCallback
        {
            void onAddProductCallback(bool isSuccess);
        }
    }
}
