﻿using MyShopData.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyShopData
{
    public class MyShopDataHelper
    {
        public static void AddProduct(int productId, String name, int qty, String photo, double price, int catId, String currency)
        {
            using (var db = new MyShopContext())
            {
                var product = db.Products.SingleOrDefault(c => c.ProductID == productId);
                if (product != null)
                {
                    product.ProductID = productId;
                    product.ProductName = name;
                    product.ProductQty = qty;
                    product.ProductPhoto = photo;
                    product.ProductPrice = price;
                    product.ProductCategoryId = catId;
                    product.UpdateDate = DateTime.Now;
                    product.Currency = currency;
                }
                else
                {
                    db.Products.Add(new Product()
                    {
                        ProductID = productId,
                        ProductName = name,
                        ProductQty = qty,
                        ProductPhoto = photo,
                        ProductPrice = price,
                        ProductCategoryId = catId,
                        CreateDate = DateTime.Now,
                        UpdateDate = DateTime.Now,
                        Currency = currency,
                    });
                }
                db.SaveChanges();
            }
        }

        public static void DeleteProduct(int productId)
        {
            using (var db = new MyShopContext())
            {
                var product = new Product { ProductID = productId };
                db.Entry(product).State = EntityState.Deleted;
                db.SaveChanges();
            }
        }

        public static List<Product> GetProducts()
        {
            using (var db = new MyShopContext())
            {
                return db.Products.ToList();
            }
        }

        public static void AddCategory(int id, String name, String status)
        {
            using (var db = new MyShopContext())
            {
                var category = db.Categories.SingleOrDefault(c => c.CategoryID == id);
                if (category != null)
                {
                    category.CategoryName = name;
                    category.UpdateDate = DateTime.Now;
                    category.Status = status;
                }
                else
                {
                    db.Categories.Add(new Category()
                    {
                        CategoryID = id,
                        CategoryName = name,
                        Status = status,
                        CreateDate = DateTime.Now,
                        UpdateDate = DateTime.Now,
                    });
                }
                db.SaveChanges();
            }
        }
        public static void DeleteCategory(int id)
        {
            using (var db = new MyShopContext())
            {
                var category = new Category { CategoryID = id };
                db.Entry(category).State = EntityState.Deleted;
                db.SaveChanges();
            }
        }

        public static List<Category> GetCategories()
        {
            using (var db = new MyShopContext())
            {
                return db.Categories.ToList();
            }
        }

        public static List<Category> GetActiveCategories()
        {
            using (var db = new MyShopContext())
            {
                return db.Categories.Where(c => c.Status == "active").ToList();
            }
        }

        public static Category GetCategoryById(int id)
        {
            using (var db = new MyShopContext())
            {
                return db.Categories.Single(c => c.CategoryID == id);
            }
        }
    }
}
