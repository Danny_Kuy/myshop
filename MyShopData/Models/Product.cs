﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyShopData.Models
{
    public class Product
    {

        [Key]
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string ProductPhoto { get; set; }
        public string Currency { get; set; }
        public int ProductCategoryId { get; set; }
        public int ProductQty { get; set; }
        public double ProductPrice { get; set; }
    }
}
