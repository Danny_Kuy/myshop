﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace MyShopData.Models
{
    public class Sell
    {
        [Key]
        public int SellID { get; set; }
        public DateTime? CreateDate { get; set; }
        public int TotalPrice { get; set; }
    }
}
