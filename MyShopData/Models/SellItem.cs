﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace MyShopData.Models
{
    public class SellItem
    {
        [Key]
        public int SellItemID { get; set; }
        public int SellID { get; set; }
        public int ProductID { get; set; }
        public int Price { get; set; }
        public int Qty { get; set; }
    }
}
