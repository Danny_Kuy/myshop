﻿using MyShopData.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyShopData
{
    public class MyShopContext : DbContext
    {
        public MyShopContext() : base("name=MyShopContext")
        {
            Database.SetInitializer<MyShopContext>(null);
        }


        public DbSet<Product> Products { get; set; }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Sell> Sells { get; set; }
        public DbSet<SellItem> SellItems { get; set; }
    }
}
